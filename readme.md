# GlobalMentor Documentation and Publishing Files

## Dependencies

- [Prism](http://prismjs.com/download.html)
	* `coy` theme
	* all languages
	* plugins
		* Line Numbers
		* Keep Markup
		* Command Line
